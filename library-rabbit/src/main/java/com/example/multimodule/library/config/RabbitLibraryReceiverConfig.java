package com.example.multimodule.library.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


@Configuration
@ComponentScan(basePackages = "com.example.multimodule.library.messaging.receiver")
@PropertySource({"classpath:rabbit-dev.properties"})
public class RabbitLibraryReceiverConfig {
    @Autowired
    Environment env;


}

