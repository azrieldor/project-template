package com.example.multimodule.library.messaging.sender;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(String queueName, String message){
        rabbitTemplate.convertAndSend(queueName,message);
        System.out.println("Message sent: " + message);
    }
}
