//package com.example.multimodule.application;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import com.example.multimodule.service.dao.MessageDao;
//import com.example.multimodule.service.repository.MessagesRepository;
//import org.junit.jupiter.api.Test;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.context.SpringBootTest;
//
//@SpringBootTest
//public class DemoApplicationTest {
//
//    @Autowired
//    MessagesRepository messagesRepository;
//
//    @Test
//    public void testDatabaseConnection (){
//        assertThat(messagesRepository).isNotNull();
//    }
//    @Test
//    public void testDatabaseCreateMessage (){
//        messagesRepository.save(new MessageDao(9999,"test"));
//        assertThat(messagesRepository.findAll().stream().filter(messageDao -> {
//            return messageDao.getId() == 9999 && messageDao.getMessage() == "test";
//        })).isNotNull();
//    }
//
//
//
//}
