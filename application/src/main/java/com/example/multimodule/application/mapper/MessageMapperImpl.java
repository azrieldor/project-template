package com.example.multimodule.application.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Scope("prototype")
public class MessageMapperImpl<T1,T2> implements MassageMapper<T1,T2>{

    private final ModelMapper modelMapper;

    @Autowired
    public MessageMapperImpl( ModelMapper modelMapper){
        this.modelMapper = modelMapper;
    }

    @Override
    public T2 map(T1 from, Class<T2> toClass) {
        return modelMapper.map(from,toClass);
    }

    @Override
    public List<T2> map(List<T1> from, Class<T2> toClass) {
        return from.stream().map(f -> map(f,toClass)).toList();
    }
}
