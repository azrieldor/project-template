package com.example.multimodule.application.controllers;//package org.tempng.controllers;

import com.example.multimodule.application.dto.MessageDto;
import com.example.multimodule.application.mapper.MassageMapper;
import com.example.multimodule.application.requests.MessageRequest;
import com.example.multimodule.application.responses.MessageResponse;
import com.example.multimodule.application.services.MessageService;
import com.example.multimodule.library.messaging.sender.MessageSender;
import com.example.multimodule.postgreslibrary.dao.ArticleDao;
//import com.example.multimodule.postgreslibrary.repository.ArticleRepository;
import com.example.multimodule.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/message")
public class HomeController {

    final private MessageService messageService;

    private final MassageMapper<MessageRequest, MessageDto> messageRequestToDtoMapper;
    private final MassageMapper<MessageDto, MessageResponse> messageDtoToResponseMapper;


    private MessageSender messageSender;
    @Autowired
    public HomeController(MessageService messageService,
                          MassageMapper<MessageRequest, MessageDto> messageRequestToDtoMapper,
                          MassageMapper<MessageDto, MessageResponse> messageDtoToResponseMapper,
                          MessageSender messageSender
                          ){
        this.messageService = messageService;
        this.messageRequestToDtoMapper = messageRequestToDtoMapper;
        this.messageDtoToResponseMapper = messageDtoToResponseMapper;
        this.messageSender = messageSender;
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createMessage(@RequestBody MessageRequest messageRequest) {
        messageSender.sendMessage("myQueue", messageRequest.toString());
        messageService.createMessage(messageRequestToDtoMapper.map(messageRequest,MessageDto.class));
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<MessageResponse> getAllMessages() {
        return messageDtoToResponseMapper.map(messageService.getAllMessages(),MessageResponse.class);
    }

}
