package com.example.multimodule.application.services;

import com.example.multimodule.application.dto.MessageDto;
import com.example.multimodule.application.mapper.MassageMapper;
import com.example.multimodule.service.dal.MessageRepositoryDal;
import com.example.multimodule.service.dao.MessageDao;
import jakarta.persistence.EntityManagerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MessageServiceImpl implements MessageService{
    private final MessageRepositoryDal messagesRepositoryDal;
    private final MassageMapper<MessageDao, MessageDto> messageDaoToDtoMapper;
    private final MassageMapper<MessageDto, MessageDao> messageDtoToDaoMapper;

    @Autowired
    public MessageServiceImpl(MessageRepositoryDal messagesRepository,
                              MassageMapper<MessageDao, MessageDto> messageDaoToDtoMapper,
                              MassageMapper<MessageDto, MessageDao> messageDtoToDaoMapper) {
        this.messagesRepositoryDal = messagesRepository;
        this.messageDaoToDtoMapper = messageDaoToDtoMapper;
        this.messageDtoToDaoMapper = messageDtoToDaoMapper;
    }
    @Override
    public void createMessage(MessageDto message) {
        MessageDao messageDao = messageDtoToDaoMapper.map(message,MessageDao.class);
        messagesRepositoryDal.save(messageDao);
        log.info("Message {} saved - ", messageDao.getId());
    }

    public List<MessageDto> getAllMessages() {
        List<MessageDao> messages = messagesRepositoryDal.findAll();
        return messageDaoToDtoMapper.map(messages,MessageDto.class);
    }
}
