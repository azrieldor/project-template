package com.example.multimodule.application.config;

import com.example.multimodule.library.config.RabbitLibrarySenderConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
//@ComponentScan(basePackages = "com.example.multimodule.application")
@Import(value = {RabbitLibrarySenderConfig.class})
public class AppConfig {


}
