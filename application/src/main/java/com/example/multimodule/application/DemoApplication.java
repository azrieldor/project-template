package com.example.multimodule.application;

import com.example.multimodule.application.utils.Color;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.util.Collections;


@SpringBootApplication(
		scanBasePackages = {
				"com.example.multimodule.application"
		},exclude = {DataSourceAutoConfiguration.class})

public class DemoApplication {

	@Bean
	public ModelMapper modelMapper (){return new ModelMapper();}

	@Bean
	public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
		return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), Collections.emptyMap(), null);
	}
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}
}
