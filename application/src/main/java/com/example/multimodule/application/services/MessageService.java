package com.example.multimodule.application.services;
import com.example.multimodule.application.dto.MessageDto;
import com.example.multimodule.service.dao.MessageDao;

import java.util.List;

public interface MessageService {
    public void createMessage(MessageDto message);
    public List<MessageDto> getAllMessages();
}
