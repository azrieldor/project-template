package com.example.multimodule.application.controllers;


import com.example.multimodule.postgreslibrary.dao.ArticleDao;
import com.example.multimodule.postgreslibrary.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/article")
public class ArticlesController {

    @Autowired
    ArticleRepository articleRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createArticle(@RequestBody ArticleDao article){
        articleRepository.save(article);

    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ArticleDao> getAllArticles(){
        return articleRepository.findAll();
    }

}
