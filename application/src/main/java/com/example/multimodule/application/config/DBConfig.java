package com.example.multimodule.application.config;

import com.example.multimodule.postgreslibrary.config.ArticlesJpaConfig;
import com.example.multimodule.service.config.MessagesJpaConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
//@ConfigurationProperties(prefix = "spring.datasource")
@Import(value = {MessagesJpaConfig.class, ArticlesJpaConfig.class})

public class DBConfig {

    private  String driverClassName;
    private  String url;
    private  String username;
    private  String password;



    @Profile("dev")
    @Bean
    public String devDatabaseConnection (){
       System.out.println("Connected to DB from DEV environment ");
       System.out.println(url);
       return "DB connected from DEV ENV";
    }

    @Profile("test")
    @Bean
    public String testDatabaseConnection (){
        System.out.println("Connected to DB from DEV environment ");
        System.out.println(url);
        return "DB connected from TEST ENV";
    }
}
