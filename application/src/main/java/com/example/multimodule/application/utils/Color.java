package com.example.multimodule.application.utils;

import lombok.Getter;

@Getter
public enum Color {
    RED("red"), GREEN("green"), YELLOW("yellow");

    private String value;
    Color(String value){
        this.value=value;
    }


}
