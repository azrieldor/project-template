package com.example.multimodule.application.mapper;

import java.util.List;

public interface MassageMapper<T1,T2> {
    public  T2 map(T1 from, Class<T2> toClass);
    public  List<T2> map(List<T1> from, Class<T2> toClass);
}
