package com.example.multimodule.service.dal;

import com.example.multimodule.service.dao.UserDao;
import com.example.multimodule.service.repository.MessagesRepository;
import com.example.multimodule.service.repository.UserRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
@Transactional(readOnly = true)
public class UserRepositoryDalImpl implements UserRepositoryDal{
    @Autowired
    @Qualifier("messagesEntityManagerFactory")
    private EntityManager em;

    @Autowired
    private UserRepository repository;

    @Override
    @Transactional
    public UserDao save(UserDao userDao) {
        return repository.save(userDao);
    }

    @Override
    public List<UserDao> findAll() {
        return repository.findAll();
    }
}
