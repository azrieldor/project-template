package com.example.multimodule.service.utils;

public class ConfigConstants {
    final public static String DATA_SOURCE = "messagesDataSource";
    final public static String ENTITY_MANAGER_REF = "messagesEntityManagerFactory";
    final public static String TRANSACTION_MANAGER_REF = "messagesEntityManagerFactory";
    final public static String CONFIGURATION_PROPERTIES = "spring.datasource.messages";
    final public static String BASE_PACKAGE = "com.example.multimodule.service";
    final public static String PROPERTY_SOURCE = "classpath:library-${spring.profiles.active}.properties";
}
