package com.example.multimodule.service.dal;

import com.example.multimodule.service.dao.MessageDao;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository

public interface MessageRepositoryDal {
    public MessageDao save(MessageDao message);

    public List<MessageDao> findAll();
}
