package com.example.multimodule.service.config;

import com.example.multimodule.service.utils.ConfigConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Objects;

@Configuration

@EnableTransactionManagement
@PropertySource(ConfigConstants.PROPERTY_SOURCE)
@EnableJpaRepositories(
        basePackages = {ConfigConstants.BASE_PACKAGE},
        entityManagerFactoryRef = ConfigConstants.ENTITY_MANAGER_REF,
        transactionManagerRef = ConfigConstants.TRANSACTION_MANAGER_REF
)

@ComponentScan(basePackages ={ConfigConstants.BASE_PACKAGE} )
@EntityScan(basePackages = {ConfigConstants.BASE_PACKAGE})
public class MessagesJpaConfig {
    @Autowired
    private Environment env;

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean messagesEntityManagerFactory(
            @Qualifier(ConfigConstants.DATA_SOURCE) DataSource dataSource,
            EntityManagerFactoryBuilder builder) {
        System.out.println(env);
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(ConfigConstants.BASE_PACKAGE);

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        properties.put("hibernate.show_sql", "true");
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    @Primary
    public PlatformTransactionManager messagesTransactionManager(
            @Qualifier(ConfigConstants.ENTITY_MANAGER_REF) LocalContainerEntityManagerFactoryBean messagesEntityManagerFactory) {
        return new JpaTransactionManager(Objects.requireNonNull(messagesEntityManagerFactory.getObject()));
    }
    @Bean
    @ConfigurationProperties(ConfigConstants.CONFIGURATION_PROPERTIES)
    public DataSourceProperties messagesDataSourceProperties() {
        return new DataSourceProperties();
    }
    @Bean
    public DataSource messagesDataSource() {
        return messagesDataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Bean
    public JdbcTemplate topicsJdbcTemplate(@Qualifier(ConfigConstants.DATA_SOURCE) DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
