package com.example.multimodule.service.repository;

import com.example.multimodule.service.dao.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserDao,Integer> {
}
