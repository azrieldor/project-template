package com.example.multimodule.service.repository;

import com.example.multimodule.service.dao.MessageDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface MessagesRepository  extends JpaRepository<MessageDao,Integer> {
}
