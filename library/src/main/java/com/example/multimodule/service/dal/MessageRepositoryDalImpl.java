package com.example.multimodule.service.dal;

import com.example.multimodule.service.dao.MessageDao;
import com.example.multimodule.service.repository.MessagesRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public class MessageRepositoryDalImpl implements MessageRepositoryDal{

    @Autowired
    @Qualifier("messagesEntityManagerFactory")
    private EntityManager em;

    @Autowired
    @Qualifier("messagesTransactionManager")
    private PlatformTransactionManager messagesTransactionManager;
    @Autowired
    private MessagesRepository repository;

    @Override
    @Transactional(transactionManager = "messagesTransactionManager")
    public MessageDao save(MessageDao message) {
        return repository.save(message);
    }
    @Override
    public List<MessageDao> findAll(){
        return repository.findAll();
    }
}
