package com.example.multimodule.service.dal;

import com.example.multimodule.service.dao.MessageDao;
import com.example.multimodule.service.dao.UserDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
public interface UserRepositoryDal {
    public UserDao save(UserDao userDao);

    public List<UserDao> findAll();
}
