package com.example.multimodule.postgreslibrary.config;

import com.example.multimodule.postgreslibrary.utils.ConfigConstants;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Objects;




@Configuration
@PropertySource(ConfigConstants.PROPERTY_SOURCE)
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = {ConfigConstants.BASE_PACKAGE},
        entityManagerFactoryRef = ConfigConstants.ENTITY_MANAGER_REF,
        transactionManagerRef = ConfigConstants.TRANSACTION_MANAGER_REF
)
@EntityScan(basePackages = {ConfigConstants.BASE_PACKAGE})
@ComponentScan(basePackages = {ConfigConstants.BASE_PACKAGE})

public class ArticlesJpaConfig {
    @Bean
    public LocalContainerEntityManagerFactoryBean articlesEntityManagerFactory(
            @Qualifier(ConfigConstants.DATA_SOURCE) DataSource dataSource,
            EntityManagerFactoryBuilder builder) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(ConfigConstants.BASE_PACKAGE);

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        properties.put("hibernate.show_sql", "true");
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    public PlatformTransactionManager articlesTransactionManager(
            @Qualifier(ConfigConstants.ENTITY_MANAGER_REF) LocalContainerEntityManagerFactoryBean articlesEntityManagerFactory) {
        return new JpaTransactionManager(Objects.requireNonNull(articlesEntityManagerFactory.getObject()));
    }
    @Bean
    @ConfigurationProperties(ConfigConstants.CONFIGURATION_PROPERTIES)
    public DataSourceProperties articlesDataSourceProperties() {
        return new DataSourceProperties();
    }
    @Bean
    public DataSource articlesDataSource() {
        return articlesDataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

}
