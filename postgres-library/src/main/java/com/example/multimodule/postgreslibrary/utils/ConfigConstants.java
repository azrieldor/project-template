package com.example.multimodule.postgreslibrary.utils;

public class ConfigConstants {
    final public static String DATA_SOURCE = "articlesDataSource";
    final public static String ENTITY_MANAGER_REF = "articlesEntityManagerFactory";
    final public static String TRANSACTION_MANAGER_REF = "articlesTransactionManager";
    final public static String CONFIGURATION_PROPERTIES = "spring.datasource.articles";
    final public static String BASE_PACKAGE = "com.example.multimodule.postgreslibrary";
    final public static String PROPERTY_SOURCE = "classpath:postgres-${spring.profiles.active}.properties";


}
