package com.example.multimodule.postgreslibrary.dao;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "articles")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ArticleDao {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

}
