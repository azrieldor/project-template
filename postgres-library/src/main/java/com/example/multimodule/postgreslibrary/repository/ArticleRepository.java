package com.example.multimodule.postgreslibrary.repository;

import com.example.multimodule.postgreslibrary.dao.ArticleDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<ArticleDao,Integer> {
}
