package com.example.multimodule.secondapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DBCinfig {

    private  String driverClassName;
    private  String url;
    private  String username;
    private  String password;



    @Profile("dev")
    @Bean
    public String devDatabaseConnection (){
       System.out.println("Connected to DB from DEV environment ");
       System.out.println(url);
       return "DB connected from DEV ENV";
    }

    @Profile("test")
    @Bean
    public String testDatabaseConnection (){
        System.out.println("Connected to DB from DEV environment ");
        System.out.println(url);
        return "DB connected from TEST ENV";
    }
}
