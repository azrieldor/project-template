package com.example.multimodule.secondapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(
		scanBasePackages = {
				"com.example.multimodule.secondapp"
		})

public class SecondApplication {




	public static void main(String[] args) {
		SpringApplication.run(SecondApplication.class, args);

	}
}
