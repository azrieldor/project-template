package com.example.multimodule.secondapp.controller;//package org.tempng.controllers;

import com.example.multimodule.library.messaging.receiver.MessageReceiver;
import com.example.multimodule.secondapp.services.UserService;
import com.example.multimodule.service.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/user")
public class HomeController {

    final private UserService userService;


    @Autowired
    public HomeController(UserService userService){
        this.userService = userService;
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@RequestBody UserDao userDao) {
        userService.createUser(userDao);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDao> getAllUsers() {
        return userService.getAllMessages();
    }

}
