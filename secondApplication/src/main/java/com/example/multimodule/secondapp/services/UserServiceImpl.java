package com.example.multimodule.secondapp.services;

import com.example.multimodule.service.dal.UserRepositoryDal;
import com.example.multimodule.service.dao.UserDao;
import com.example.multimodule.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepositoryDal userRepository;


    @Override
    public void createUser(UserDao user) {
        userRepository.save(user);
    }

    @Override
    public List<UserDao> getAllMessages() {
        return userRepository.findAll();
    }
}
