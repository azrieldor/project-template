package com.example.multimodule.secondapp.services;

import com.example.multimodule.service.dao.UserDao;

import java.util.List;

public interface UserService {
    public void createUser(UserDao message);
    public List<UserDao> getAllMessages();
}
