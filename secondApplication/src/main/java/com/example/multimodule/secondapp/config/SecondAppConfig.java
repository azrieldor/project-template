package com.example.multimodule.secondapp.config;

import com.example.multimodule.library.config.RabbitLibraryReceiverConfig;
import com.example.multimodule.library.messaging.receiver.MessageReceiver;
import com.example.multimodule.service.config.LibraryConfig;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {LibraryConfig.class , RabbitLibraryReceiverConfig.class})
public class SecondAppConfig {
    @Autowired
    MessageReceiver messageReceiver;
    @Bean
    public Queue myQueue() {
        return new Queue("myQueue", true); // Declare a durable queue
    }

}
